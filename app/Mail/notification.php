<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class notification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data=[];
    public function __construct($payload)
    {
        //
        $this->data=$payload;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => env('APP_EMAIL'), 'name' => env('APP_EMAIL_NAME')])->view('mails.notification')->subject(' ----NOTIFICATION----')->with($this->data);
    }
}
