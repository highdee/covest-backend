<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deposit extends Model
{
    //

    protected $appends=['fixedSaving'];

    public function getFixedSavingAttribute(){
        return $this->fixedSaving()->first();
    }

    public function fixedSaving(){
        return $this->belongsTo('App\fixedSavings','savings_id','id');
    }
}
