<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fixed extends Model
{
    // public function fixedSaving(){
    //     return $this->belongsTo('App/fixedSaving', 'fixed_savings_id', 'id');
    // }
    public function fixedTransactions(){
        return $this->hasMany('App\fixedTransaction', 'saving_id', 'id');
    }
}
