<?php

namespace App\Http\Controllers;

use App\card;
use App\stash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class cardController extends Controller
{
    //
    public function __construct(){
        $this->middleware('jwt.auth');
    }

    public function add(Request $request){
        $data=$request->input();


        $card=new card();
        $card->user_id=Auth::user($request->query('token'))->id;
        $card->account_name=$data['account_name'] == null ? '':$data['account_name'];
        $card->last4=$data['last4'];
        $card->exp_month=$data['exp_month'];
        $card->exp_year=$data['exp_year'];
        $card->reference=$data['reference'];
        $card->card_type=$data['card_type'];
        $card->customer_code=$data['customer_code'];
        $card->authorization_code=$data['authorization_code'];
        $card->status=1;

        $card->save();

        $stash=stash::where(['user_id'=>$card->user_id])->first();
        if(!$stash){
            $stash=new stash();
            $stash->user_id=$card->user_id;
            $stash->amount=0;
        }
        $stash->amount=$stash->amount+10;
        $stash->save();

        return response()->json([
            'status'=>true,
            'message'=>'Card added succesfully.',
            'data'=>$card
        ]);

    }

    public function getCards(Request $request){
        $cards=card::where('user_id',Auth::user($request->query('token'))->id)->get();
        return $cards;
    }

    public function deleteCard($id){
        $card=card::find($id);
        if(!$card){
            return response()->json([
                'status'=>false,
                'message'=>'Card not found'
            ]);
        }

        $card->delete();
        return response()->json([
            'status'=>true,
            'message'=>'Card removed successfully'
        ]);
    }
}
