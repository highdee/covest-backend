<?php

namespace App\Http\Controllers;

use App\banks;
use App\investment;
use App\invest_transactions;
use App\card;
use App\Mail\investmentCreationMail;
use App\Mail\notification;
use App\tenor;
use App\User;
use App\withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class investmentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['']);
    }

    public function getTenors(){
        $user=Auth::user();
        $tenors=tenor::all();

        return $tenors;
    }

    public function create_investment(Request $request){
        $this->validate($request,[
            'name'=>'required|max:50',
            'amount'=>'required|numeric',
            'tenor'=>'required|max:20',
            'reference'=>'required_without:card'
        ]);

        $data=$request->input();

        $tenor=tenor::find($data['tenor']);


        if(!$tenor){
            return response([
                'status'=>false,
                'message'=>'Tenor is invalid'
            ]);
        }


        if($data['amount'] < 100000){
            return response([
                'status'=>false,
                'message'=>'Minimum investment is 100000'
            ]);
        }

        $user=Auth::user();

        if(isset($data['reference'])){
            $headers = array(
                'Authorization: Bearer '.env('SECRET_KEY'),
//                'Content-type: Application/json'
            );

            $url='https://api.paystack.co/transaction/verify/'.$data['reference'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST,false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result=curl_exec($ch);
            curl_close($ch);
            $payload=json_decode($result);


            if($payload->status){ //if txn was successful

                $investment=new investment();
                $investment->label=$data['name'];
                $investment->user_id=$user->id;
                $investment->amount=$data['amount'];
                $investment->tenor=$tenor->title;
                $investment->data=json_encode($tenor);
                $investment->ref=$data['reference'];
                $investment->status=0;

                $investment->save();

                $txn=new invest_transactions();
                $txn->amount=$payload->data->amount;
                $txn->text='made an investment of '.$payload->data->amount;
                $txn->invest_id=$investment->id;
                $txn->type='Credit';
                $txn->status=1;
                $txn->reference=$payload->data->reference;
                $txn->save();

                $data=[
                    'name'=> $user->lastname,
                    'investment'=> $investment
                ];
//
                Mail::to($user->email)->send(new investmentCreationMail($data));

                return response([
                    'status'=>true,
                    'message'=>'Investment was created successfully',
                    'data'=>$investment
                ]);

            }else{
                //save this error in transaction

                return response([
                    'status'=>false,
                    'message'=>'Invalid transaction reference please contact the admin',
                    'retry'=>true
                ]);
            }
        }
        else if(isset($data['card'])){
            $card=card::find($data['card']);


            if(!$card){
                return response([
                    'status'=>false,
                    'message'=>"Please select a valid card",
                ],200);
            }


            $headers = array(
                'Authorization: Bearer '.env('SECRET_KEY'),
                'Content-type: Application/json'
            );
            $fields=[
                'authorization_code'=>$card->authorization_code,
                'amount'=>$data['amount'],
                'email'=>$user->email,
                'send_invoices'=>true
            ];
            $url='https://api.paystack.co/transaction/charge_authorization';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result=curl_exec($ch);
            curl_close($ch);
            $payload=json_decode($result);


            if(!$payload){
                return response([
                    'status'=>false,
                    'message'=>"An error occured while verifying payment.Please report to our team",
                ],200);
            }

            if($payload->status){ //if txn was successful
                $investment=new investment();
                $investment->label=$data['name'];
                $investment->user_id=$user->id;
                $investment->tenor=$tenor->title;
                $investment->data=json_encode($tenor);
                $investment->ref=$payload->data->reference;
                $investment->status=0;
                $investment->amount=$payload->data->amount;
                $investment->save();

                $txn=new invest_transactions();
                $txn->amount=$payload->data->amount;
                $txn->text='made a savings of '.$payload->data->amount;
                $txn->invest_id=$investment->id;
                $txn->type='Credit';
                $txn->status=1;
                $txn->reference=$payload->data->reference;
                $txn->save();

                $data=[
                    'name'=> $user->lastname,
                    'investment'=> $investment
                ];
//
                Mail::to($user->email)->send(new investmentCreationMail($data));
            }else{
                $txn=new invest_transactions();
                $txn->amount=$payload->data->amount;
                $txn->text='error occured while making payment of '.$payload->data->amount;
                $txn->invest_id=0;
                $txn->type='Credit';
                $txn->status=2;
                $txn->reference=$payload->data->reference;
                $txn->save();
            }

            return response([
                'status'=>true,
                'message'=>"Payment was successful",
                'data'=>$investment
            ],200);
        }

    }

    public function invest_withdraw(Request $request){
        $this->validate($request,[
            'amount'=>'required',
            'bank_name'=>'required',
            'account_number'=>'required',
            'pin'=>'required',
            'investment'=>'required',
        ]);

        $user=Auth::user();
        $data=$request->input();

        $investment=investment::find($data['investment']);

        if(!password_verify($data['pin'],$user->covest_password)){
            return response([
                'status'=>false,
                'message'=>"Invalid pin"
            ]);
        }


        $check=withdrawal::where(['user_id'=>$user->id,'type' => 'invest','status'=>0])->first();
        if($check){
            return response([
                'status'=>false,
                'message'=>"There is an on-going withdrawal transaction. You can't process a new one until the current one is done."
            ]);
        }

        $bank=banks::find($data['bank_name']);

        if(!$bank){
            return response([
                'status'=>false,
                'message'=>"we dont recognise your bank"
            ]);
        }

        $wid=new withdrawal();
        $wid->user_id=$user->id;
        $wid->type='invest';
        $wid->account_id=$investment->id;
        $wid->amount=(float)$investment->amount+(float)$investment->profit;
        $wid->status=0;
        $wid->bank=$data['bank_name'];
        $wid->account_number=$data['account_number'];

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );

        $fields=[
            'type'=>'nuban',
            'name'=>'',
            'account_number'=>$data['account_number'],
            'bank_code'=>$bank->code,
        ];
        $url='https://api.paystack.co/transferrecipient';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $res=json_decode($result);

        if(!$res->status){
            return response([
                'status'=>false,
                'message'=>$res->message
            ]);
        }

        $wid->recipient_code=$res->data->recipient_code;


        $data=[
            'user'=> $user->lastname,
            'text'=>"
                <p>You have made a withdrawal request of <br>NGN".$data['amount']."</br> from your stash account.</p> 
                <p>Your withdrawal request will be reviewed and we will get back to you shortly.</p>
            "
        ];
        Mail::to($user->email)->send(new notification($data));

        $wid->save();
        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>'Withdrawal request was submitted successfully',
            'data'=> $user
        ]);
    }

    public function get_investment(){
        $user=Auth::user();

        $invesments=investment::where(['user_id'=>$user->id])->where('status','!=',-1)->where('status','!=',2)->get();

        return $invesments;
    }
}
