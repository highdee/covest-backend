<?php

namespace App\Http\Controllers;

use App\banks;
use App\card;
use App\fixed;
use App\fixedTransaction;
use App\Mail\notification;
use App\stash;
use App\stash_transactions;
use App\withdrawal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class stashSavingsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['generateDeposit']);
    }

    public function getStash(){
        $user=Auth::user();
        $stash=stash::where('user_id',$user->id)->first();

        if(!$stash){
            return response([
                'status'=>false,
                'message'=>"Stash not found",
            ],404);
        }

        return $stash;
    }

    public function createStash(){
        $user=Auth::user();
        $stash=stash::where('user_id',$user->id)->first();

        if($stash){
            return response([
                'status'=>false,
                'message'=>"Stash savings already exist for this account",
            ],200);
        }

        $stash=new stash();
        $stash->user_id=$user->id;
        $stash->amount=0;
        $stash->save();

        return response([
            'status'=>true,
            'message'=>"Stash created successfully",
            'data'=>$stash
        ],200);
    }

    public function getStashSavingTxn($id,$offset=0){
        $txns=stash_transactions::where(['stash_id'=>$id])->offset($offset)->orderBy('id','DESC')->paginate();

        return $txns;
    }

    public function saveStashSaving(Request $request){
        $details=$request->input();
        $user=Auth::user();

        $stash=stash::where('user_id',$user->id)->first();

        if(!$stash){
            return response([
                'status'=>false,
                'message'=>"Stash savings account was not found.",
            ],200);
        }

        if(empty($details['amount'])){
            return response([
                'status'=>false,
                'message'=>"Please enter an amount",
            ],200);
        }

        if(empty($details['card'])){
            return response([
                'status'=>false,
                'message'=>"Please select a card",
            ],200);
        }

        $card=card::find($details['card']);


        if(!$card){
            return response([
                'status'=>false,
                'message'=>"Please select a valid card",
            ],200);
        }


        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );
        $fields=[
            'authorization_code'=>$card->authorization_code,
            'amount'=>$details['amount'] ,
            'email'=>$user->email,
            'send_invoices'=>true
        ];
        $url='https://api.paystack.co/transaction/charge_authorization';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);


        if(!$data){
            return response([
                'status'=>false,
                'message'=>"An error occured while verifying payment.Please report to our team",
            ],200);
        }

        if($data->status){ //if txn was successful
            $stash->amount+=$data->data->amount;
            $stash->save();

            $txn=new stash_transactions();
            $txn->amount=$data->data->amount;
            $txn->text='made a savings of '.$data->data->amount;
            $txn->stash_id=$stash->id;
            $txn->type='Credit';
            $txn->status=1;
            $txn->reference=$data->data->reference;
            $txn->save();
        }else{
            $txn=new stash_transactions();
            $txn->amount=$data->data->amount;
            $txn->text='error occured while making payment of '.$data->data->amount;
            $txn->stash_id=$stash->id;
            $txn->type='Credit';
            $txn->status=2;
            $txn->reference=$data->data->reference;
            $txn->save();
        }

        return response([
            'status'=>true,
            'message'=>"Payment was successful",
            'data'=>$stash
        ],200);
    }

    public function recordSaved($ref){
        $user=Auth::user();

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
//            'Content-type: Application/json'
        );

        $url='https://api.paystack.co/transaction/verify/'.$ref;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);

//        return json_encode($result);

        if($data->status){ //if txn was successful
            $data->data->amount=$data->data->amount / 100;
            $stash=stash::where('user_id',$user->id)->first();
            $stash->amount=(float)$stash->amount + (float)$data->data->amount;
            $stash->save();

            $txn=new stash_transactions();
            $txn->amount=$data->data->amount;
            $txn->text='made a savings of '.$data->data->amount;
            $txn->stash_id=$stash->id;
            $txn->type='Credit';
            $txn->status=1;
            $txn->reference=$data->data->reference;
            $txn->save();

            return response([
                'status'=>true,
                'message'=>"Payment was successful",
                'data'=>$stash
            ]);
        }else{
            //save this error in transaction
        }

        return response([
            'status'=>false,
            'message'=>"Sorry we are unable to process your transaction at this time. Please contact our customer care with the reference number ".$ref,
        ],200);
    }

    public function withdraw(Request $request){
        $this->validate($request,[
            'amount'=>'required',
            'bank_name'=>'required',
            'account_number'=>'required',
            'pin'=>'required'
        ]);

        $user=Auth::user();
        $stash=stash::find($user->stash->id);
        $data=$request->input();

        if(!password_verify($data['pin'],$user->covest_password)){
            return response([
                'status'=>false,
                'message'=>"Invalid pin"
            ]);
        }


        $check=withdrawal::where(['user_id'=>$user->id,'type' => 'stash','status'=>0])->first();
        if($check){
            return response([
                'status'=>false,
                'message'=>"There is an on-going withdrawal transaction. You can't process a new one until the current one is done."
            ]);
        }

        if(!$stash->amount >= $data['amount']){
            return response([
                'status'=>false,
                'message'=>'Insufficient fund'
            ]);
        }

        $bank=banks::find($data['bank_name']);

        if(!$bank){
            return response([
                'status'=>false,
                'message'=>"we dont recognise your bank"
            ]);
        }

        $wid=new withdrawal();
        $wid->user_id=$user->id;
        $wid->type='stash';
        $wid->account_id=$stash->id;
        $wid->amount=$data['amount'];
        $wid->bank=$data['bank_name'];
        $wid->account_number=$data['account_number'];
        $wid->status=0;

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );

        $fields=[
            'type'=>'nuban',
            'name'=>'',
            'account_number'=>$data['account_number'],
            'bank_code'=>$bank->code,
        ];
        $url='https://api.paystack.co/transferrecipient';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $res=json_decode($result);

        if(!$res->status){
            return response([
                'status'=>false,
                'message'=>$res->message
            ]);
        }

        $wid->recipient_code=$res->data->recipient_code;

        $data=[
            'user'=> $user->lastname,
            'text'=>"
                <p>You have made a withdrawal request of <br>NGN".$data['amount']."</br> from your stash account.</p> 
                <p>Your withdrawal request will be reviewed and we will get back to you shortly.</p>
            "
        ];
        Mail::to($user->email)->send(new notification($data));

        $wid->save();
        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>'Withdrawal request was submitted successfully',
            'data'=> $user
        ]);

    }

    public function cancelWithdraw(Request $request,$id){
        $data=$request->input();
        $user=Auth::user($request->query('token'));

        $withd=withdrawal::where(['account_id'=>$id,'type'=>'stash','status'=>0])->first();

        if(!$withd){
            return response([
                'status'=>false,
                'message'=>"No withdrawal found"
            ]);
        }

        $withd->status=3; //3 means it was canceled by user

        $data=[
            'user'=> $user->lastname,
            'text'=>"
                <p>You have canceled your withdrawal request for stash account</p>
            "
        ];
        Mail::to($user->email)->send(new notification($data));

        $withd->save();

        return response([
            'status'=>true,
            'message'=>"Done"
        ]);
    }
}
