<?php

namespace App\Http\Controllers;

use App\banks;
use App\card;
use App\fixed;
use App\fixedTransaction;
use App\stash_transactions;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\fixedSavings;
use App\stash;
use App\withdrawal;
use App\tenor;
use App\investment;

class adminController extends Controller
{
    // 0-neither
    // 3 user cancel
    // -2 admin cancel
    // 1 admin made payment

    // invsetment
    // 0-active investment
    // 1-investment is ready to be paid
    // 2- completed
    // -2-Admin canceled investment
    // 3-User cancel investment



    public function __construct()
    {
        $this->middleware('auth:admin');
    } 
    public function logout() {
        Auth::logout();
        return redirect('/covestAdmin-page/adminLogin');
      }
    public function index(){
        $u = count(User::all());
        // dd($u);
        return view('admin.index')->with('user',$u);
    }
    public function userList(){
        $u = User::all();
        return view('admin.user')->with('users',$u);
    }
    public function user_show($id){
        $u = User::find($id);
        // $d=fixedSavings::where('user_id',$id)->get();
        // dd($d[0]->deposits);
        // dd($u->fixedSavings[0]->deposits[0]->deposit_day);
        // dd($u->stash->stash_transactions[0]->type);
        // dd($u->stash);
        // dd($u->withdrawals[0]->fixedAccount->fixed->amount);
        // dd($u->investments);
        return view('admin.user_show')->with('user', $u);
    }
    public function deleteUser($id){
        $u = User::find($id);
        if(!$u->delete()){
            session()->flash('message', 'User Delete Unsuccessfull');
            session()->flash('type', 'error');
            return redirect()->back();  
        }
        session()->flash('message', 'User Delete Successfull');
        session()->flash('type', 'success');
        return redirect()->route('admin.user'); 
    }

    public function banUser(Request $request){
        $u = User::find($request['user_id']);
        $u->status = 1 - $u->status;
        if(!$u->save()){
            session()->flash('message', 'User Ban Unsuccessfull');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        session()->flash('message', 'Successful');
        session()->flash('type', 'success');
        return redirect()->back(); 
    }

    public function fixedList(){
        $f=fixedSavings::orderBy('created_at', 'desc')->get();
        // dd($f[0]->user);
        return view('admin.fixed')->with('fixed', $f);
    }
    public function stashList(){
        $f=stash::orderBy('created_at', 'desc')->get();
        // dd($f[0]->user);
        return view('admin.stash')->with('stash', $f);
    }
    public function withdrawalList(){
        $f=withdrawal::orderBy('created_at', 'desc')->get();
        return view('admin.withdrawal')->with('withdrawal', $f);
    }
    public function withdrawal(Request $request){
        $w=withdrawal::find($request->input('withdraw_id'));
        if(!$w ){
            session()->flash('message', 'Withdrawal was not found');
            session()->flash('type', 'error');
            return redirect()->back();
        }

        if( $w->status != 0){
            session()->flash('message', 'Withdrawal Has already been Cancelled');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }

        if($w->type == 'invest') {
            $investment = investment::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();

            if (!$investment) {
                session()->flash('message', 'Investment was not found');
                session()->flash('type', 'error');
                return redirect()->back();
            }
            if ($investment->status != 1) {
                session()->flash('message', 'Investment was not found');
                session()->flash('type', 'error');
                return redirect()->back();
            }
            if($investment->amount < $w->amount){
                session()->flash('message', 'Insufficient fund');
                session()->flash('type', 'error');
                return redirect()->back();
            }
        }
        else if($w->type == 'stash'){
            $stash = stash::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();

            if (!$stash) {
                session()->flash('message', 'Stash was not found');
                session()->flash('type', 'error');
                return redirect()->back();
            }

            if($stash->amount < $w->amount){
                session()->flash('message', 'Insufficient fund');
                session()->flash('type', 'error');
                return redirect()->back();
            }
        }
        else if($w->type == 'fixed'){
            $fixedSavings = fixedSavings::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();

            if (!$fixedSavings) {
                session()->flash('message', 'Fixed account was not found');
                session()->flash('type', 'error');
                return redirect()->back();
            }

            $fixed = fixed::where(['fixed_savings_id' => $fixedSavings->id, 'user_id' => $w->user_id])->first();
            if($fixed->amount < $w->amount){
                session()->flash('message', 'Insufficient fund');
                session()->flash('type', 'error');
                return redirect()->back();
            }
        }

        $user=User::find($w->user_id);

        $amount=$w->amount;

        if($w->type == 'fixed'){
            $fixedSavings = fixedSavings::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();
            if($fixedSavings->since < 1){
                $amount=floor((3/100) * $w->amount);
                $amount=$w->amount - $amount;
            }
        }

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );
//
        $fields=[
            'source'=>'balance',
            'amount'=>$amount * 100,
            'currency'=>'NGN',
            'recipient'=>$w->recipient_code,
        ];
        $url='https://api.paystack.co/transfer';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);

        if(!$data->status){
            session()->flash('message', $data->message);
            session()->flash('type', 'error');
            return redirect()->back();
        }

        session()->put('withdraw', $w->id);
        session()->put('code', $data->data->transfer_code);

        return view('admin.confirm-withdrawal');


    }

    public function withdrawalToken(Request $request){
        $code=session()->get('code');
        $withdraw=session()->get('withdraw');
        $otp=$request->input('code');

        $w=withdrawal::find($withdraw);
        if(!$w ){
            session()->flash('message', 'Withdrawal was not found');
            session()->flash('type', 'error');
            return redirect()->back();
        }

        if( $w->status != 0){
            session()->flash('message', 'Withdrawal Has already been Cancelled');
            session()->flash('type', 'error');
            return redirect()->back();
        }

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );
//
        $fields=[
            'transfer_code'=>$code,
            'otp'=>$otp,
        ];



        $url='https://api.paystack.co/transfer/finalize_transfer';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);

        if(!$data->status){
            session()->flash('message', $data->message);
            session()->flash('type', 'error');
            return view('admin.confirm-withdrawal');
        }
        $w->status=1;
        $w->save();

        if($w->type == 'invest') {
            $investment = investment::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();

            if (!$investment) {
                session()->flash('message', 'Investment was not found');
                session()->flash('type', 'error');
            }
            $investment->status=2;
            $investment->amount=0;
            $investment->save();
        }
        else if($w->type == 'stash'){
            $stash = stash::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();

            if (!$stash) {
                session()->flash('message', 'Investment was not found');
                session()->flash('type', 'error');
            }
            $stash->amount -= $w->amount;
            $stash->save();

            $txn=new stash_transactions();
            $txn->amount=$w->amount;
            $txn->text='made a withdrawal of '.$w->amount;
            $txn->stash_id=$stash->id;
            $txn->type='Debit';
            $txn->status=1;
            $txn->reference='';
            $txn->save();
        }
        else if($w->type == 'fixed'){
            $fixedSavings = fixedSavings::where(['id' => $w->account_id, 'user_id' => $w->user_id])->first();

            if (!$fixedSavings) {
                session()->flash('message', 'Fixed account was not found');
                session()->flash('type', 'error');
                return redirect()->back();
            }

            $fixed = fixed::where(['fixed_savings_id' => $fixedSavings->id, 'user_id' => $w->user_id])->first();

            if (!$fixed) {
                session()->flash('message', 'Fixed account was not found');
                session()->flash('type', 'error');
                return redirect()->back();
            }



            $fixed->amount -= $w->amount;
            $fixed->save();

            $txn=new fixedTransaction();
            $txn->amount=$data->data->amount;
            $txn->text='made a withdrawal of '.$w->amount;
            $txn->saving_id=$fixedSavings->id;
            $txn->type='Credit';
            $txn->status=2;
            $txn->reference='';
            $txn->save();
        }

        session()->remove('withdraw');
        session()->remove('code');

        session()->flash('message', $data->message);
        session()->flash('type', 'success');
        return redirect()->to('admin/user_show/'.$w->user_id);
    }

    public function withdrawInvestment(Request $request){
        $i=investment::find($request->invest_id);
        if(!$i){
            session()->flash('message', 'Investment not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }

        if($i->status != 1){
            session()->flash('message', 'Investment is not yet ready to be paid');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }

        //Code to pay ......
        dd('Code to pay investment goes here');

        $i->status = 2;
        
    }

    public function cancelInvestment(Request $request){
        $i=investment::find($request->invest_id);
        if(!$i){
            session()->flash('message', 'Investment not found');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        $i->status = -2;
        
        if (!$i->save()) {
            session()->flash('message', 'Error Cancelling Investment, Pls Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }

        session()->flash('message', 'Investment Cancelled by Admin Success');
        session()->flash('type', 'success');
        return redirect()->back();

    }

    public function cancelWithdrawal(Request $request){
        $this->validate($request, [
            'withdraw_id'=>'required'
        ]);

        $w=withdrawal::find($request->withdraw_id);
        if(!$w){
            session()->flash('message', 'Withdrawal Request not found');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        $w->status = -2; //admin cancel withdrawal

        if (!$w->save()) {
            session()->flash('message', 'Error Cancelling Withdrawal, Pls Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }

        session()->flash('message', 'Withdrawal Cancelled by Admin Success');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function tenors(){
        $t = tenor::all();
        return view('admin.tenor')->with('tenor',$t);
    }

    public function create_tenor(Request $request){
        $this->validate($request, [
            'title'=>'required|string',
            'percentage'=>'required|numeric|min:0|max:100',
            'months'=>'required|numeric|min:1|max:12',
            'description'=>'required'
        ]);

        $t = new tenor();
        $t->title = $request->title;
        $t->percentage=$request->percentage;
        $t->months=$request->months;
        $t->description=$request->description;

        if(!$t->save()){
            session()->flash('message', 'Error Creating Tenor, Pls Try again');
            session()->flash('type', 'error');
            return redirect()->back();   
        }

        session()->flash('message', 'Tenor Created Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function investment(){
        $i = investment::all();
        // dd($i[0]->user);
        return view('admin.investment')->with('investment', $i);
    }
}
