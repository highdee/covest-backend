<?php

namespace App\Http\Controllers;

use App\card;
use App\fixed;
use App\investment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class userController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function get_user(Request $request){
        return response()->json(
            [
                'user'=>Auth::user($request->query('token')),
//                'token'=>JWTAuth::refresh($request->query('token'))
            ]
        );
    }

    public function get_user_card(Request $request){
        $cards=card::where('user_id',Auth::user($request->query('token')->id))->get();

        return response()->json($cards);
    }

    public function reset_token($id){

        $token = JWTAuth::getToken();
        if(!$token){
           return response([],402);
        }
        try{
            $token = JWTAuth::refresh($token);
        }catch(TokenInvalidException $e){
            return response([],402);
        }

        $user=User::find($id);
        return response()->json(compact(['token','user']));

    }

    public function get_dashboard(Request $request){
        $user=User::where('id',Auth::user($request->query('token'))->id)->first();
        $fixedaccounts=fixed::where('user_id',$user->id)->get();
        $fixedSum=0;
        foreach ($fixedaccounts as $fd){
            $fixedSum+=$fd->amount;
        }

        $investments=investment::where(['user_id'=>$user->id])->where('status','!=',-1)->where('status','!=',1)->get();
        $investment=0;
        foreach ($investments as $fd){
            (float) $investment+= (float) $fd->amount + (float)$fd->profit();
        }

        return response([
            'user'=>$user,
            'fixed'=>$fixedSum,
            'investment_bal'=>$investment
        ],200);
    }

    public function set_pin(Request $request){
        $this->validate($request,[
            'pin'=>'required',
            'password'=>'required'
        ]);

        $data=$request->input();

        $user=Auth::user();

        if(!password_verify($data['password'],$user->password)){
            return response([
                'status'=>false,
                'message'=>'Password is incorrect'
            ]);
        }

        $user->covest_password=bcrypt($data['pin']);
        $user->save();

        $user=User::find($user->id);
        return response([
            'status'=>true,
            'message'=>'Pin set successfully.',
            'data'=>$user
        ]);
    }

    public function change_password(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed'
        ]);

        $data=$request->input();

        $user=Auth::user();

        if(empty($data['old'])){
            return response([
                'status'=>false,
                'message'=>'Enter your current password'
            ]);
        }

        if(!password_verify($data['old'],$user->password)){
            return response([
                'status'=>false,
                'message'=>'Current Password is incorrect'
            ]);
        }

        $user->password=bcrypt($data['password']);
        $user->save();

        $user=User::find($user->id);
        return response([
            'status'=>true,
            'message'=>'Password set successfully.',
            'data'=>[
                'user'=>$user,
                'token'=>JWTAuth::refresh($request->query('token'))
            ]
        ]);
    }

    public function update_profile(Request $request){
        $this->validate($request,[
            'firstname'=>'required',
            'lastname'=>'required',
            'phone'=>"required"
        ]);

        $data=$request->input();

        $user=Auth::user();

        $user->firstname=$data['firstname'];
        $user->lastname=$data['lastname'];
        $user->phone=$data['phone'];
        $user->save();

        $user=User::find($user->id);
        return response([
            'status'=>true,
            'message'=>'Profile updated successfully.',
            'data'=>[
                'user'=>$user,
                'token'=>JWTAuth::refresh($request->query('token'))
            ]
        ]);
    }
}
