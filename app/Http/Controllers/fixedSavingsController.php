<?php

namespace App\Http\Controllers;

use App\banks;
use App\deposit;
use App\fixed;
use App\fixedSavings;
use App\fixedTransaction;
use App\transaction;
use App\withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Mail\notification;
use Illuminate\Support\Facades\Mail;

class fixedSavingsController extends Controller
{
    // status 2 = FINISHED Savings
    // status 1 = ACTIVE Savings
    // status 0 = PAUSES Savings
    // status -1 = CANCELED Savings


    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['']);
    }

    public function withdraw(Request $request){
        $this->validate($request,[
            'amount'=>'required',
            'bank_name'=>'required',
            'account_number'=>'required',
            'pin'=>'required'
        ]);

        $user=Auth::user();
        $data=$request->input();
        $fixed=fixedSavings::where(['user_id'=>$user->id,'id'=>$data['id']])->first();

        if(!$fixed){
            return response([
                'status'=>false,
                'message'=>"Not allowed"
            ],401);
        }

        if(!password_verify($data['pin'],$user->covest_password)){
            return response([
                'status'=>false,
                'message'=>"Invalid pin"
            ]);
        }


        $check=withdrawal::where(['user_id'=>$user->id,'type' => 'fixed','status'=>0])->first();
        if($check){
            return response([
                'status'=>false,
                'message'=>"There is an on-going withdrawal transaction. You can't process a new one until the current one is done."
            ]);
        }

        if(!$fixed->fixed->amount > $data['amount']){
            return response([
                'status'=>false,
                'message'=>'Insufficient fund'
            ]);
        }

        $bank=banks::find($data['bank_name']);

        if(!$bank){
            return response([
                'status'=>false,
                'message'=>"we dont recognise your bank"
            ]);
        }


        $wid=new withdrawal();
        $wid->user_id=$user->id;
        $wid->type='fixed';
        $wid->account_id=$fixed->id;
        $wid->amount=$data['amount'];
        $wid->status=0;
        $wid->bank=$data['bank_name'];
        $wid->account_number=$data['account_number'];


        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );

        $fields=[
            'type'=>'nuban',
            'name'=>'',
            'account_number'=>$data['account_number'],
            'bank_code'=>$bank->code,
        ];
        $url='https://api.paystack.co/transferrecipient';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $res=json_decode($result);

        if(!$res->status){
            return response([
                'status'=>false,
                'message'=>$res->message
            ]);
        }

        $wid->recipient_code=$res->data->recipient_code;


        $data=[
            'user'=> $user->lastname,
            'text'=>"
                <p>You have made a withdrawal request of <br>NGN".$data['amount']." from your ".$fixed->title." fixed account.</p> 
                <p>Your withdrawal request will be reviewed and we will get back to you shortly.</p>
            "
        ];

        Mail::to($user->email)->send(new notification($data));

        $wid->save();

        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>'Withdrawal request was submitted successfully',
            'data'=> $user
        ]);

    }

    public function create(Request $request){
        $this->validate($request,[
            'label'=>'required',
            'amount'=>'required|numeric',
            'card'=>'required',
            'plan'=>'required',
            'time'=>'required',
            'withdrawal_date'=>'required'
        ]);

        $fixed=new fixedSavings();
        $user=Auth::user();

        $data=$request->input();

        $fixed->user_id=$user->id;
        $fixed->title=$data['label'];
        $fixed->amount=$data['amount'];
        $fixed->card_id=$data['card'];
        $fixed->plan=$data['plan'];
        $fixed->plan_date=$data['plan_date'];
        $fixed->plan_day=$data['plan_day'];
        $fixed->time=$data['time'];
        $fixed->withdrawal_date=$data['withdrawal_date'];
        $fixed->status=1;
        $fixed->save();

        $account=new fixed();
        $account->user_id=$user->id;
        $account->fixed_savings_id=$fixed->id;
        $account->amount=0;
        $account->save();

        $this->generateDeposit($fixed->id);

        $fixedSavings=fixedSavings::find($fixed->id);

        if($fixedSavings->next_withdrawal == null){
            $fixedSavings->delete();
            $account->delete();

            return response([
                'status'=>false,
                'message'=>'Your withdrawal date is sooner that the first withdrawal .Please adjust your configuration.',
            ]);
        }else{
            return response([
                'status'=>true,
                'message'=>'Fixed account was saved successfully',
                'data'=>$fixed
            ]);
        }
    }

    public function generateDeposit($id){
        //this method will generate the next time that a withdrawal should occur base on the setting of the user
        $fixedSavings=fixedSavings::find($id);

        $next_withdrawal=null;

        if($fixedSavings->status == 1){
            if($fixedSavings->plan == 'daily'){
                $next_withdrawal=$this->getNextDay($fixedSavings);
            }else if($fixedSavings->plan == 'weekly'){
                $next_withdrawal=$this->getNextweek($fixedSavings);
            }else if($fixedSavings->plan == 'monthly'){
                $next_withdrawal=$this->getNextMonth($fixedSavings);
            }

            //checking to be sure the next withdrawal day is not beyond the saving withdrawal_Date
            $w_d=strtotime($fixedSavings->withdrawal_date);
            $n_w=strtotime($next_withdrawal);
            if($w_d >= $n_w){
                //updating the next withdrawal date for the fixedsavings
                $fixedSavings->status = 1;
                $fixedSavings->next_withdrawal=$next_withdrawal;
                $fixedSavings->save();

                $deposits=deposit::where("savings_id",$fixedSavings->id)->get();
                foreach ($deposits as $dp){
                    $dp->delete();
                }

                //creating a deposit for the next withdrawal
                $deposit=new deposit();
                $deposit->savings_id=$id;
                $deposit->deposit_day=$next_withdrawal;
                $deposit->deposit_at=$fixedSavings->time;
                $deposit->save();

            }else{
                $fixedSavings->status = 2;
                $fixedSavings->save();
            }
        }
    }

    public function getNextDay($data){
        $started=strtotime(date('Y-m-d',strtotime($data->created_at.'+1 day')));
        $current=strtotime(date('Y-m-d'));
        $f_t= $current > $started ? date('Y-m-d'):$data->created_at;

        $last_withdrawal=$data->last_withdrawal == null ? $f_t :$data->last_withdrawal;
        $next_withdrawal=date('Y-m-d',strtotime($last_withdrawal.'+1 day'));

        return $next_withdrawal;
    }

    public function getNextweek($data){
        $last_withdrawal=$data->last_withdrawal == null ? date('Y-m-d'):$data->last_withdrawal;
        $next_withdrawal=date('Y-m-d',strtotime($last_withdrawal.'+1 week'));

        return $next_withdrawal;
    }

    public function getNextMonth($data){
        $last_withdrawal=$data->last_withdrawal == null ? date('Y-m-d'):$data->last_withdrawal;
        $next_withdrawal=date('Y-m-d',strtotime($last_withdrawal.'+1 month'));

        return $next_withdrawal;
    }

//    public function charge(){
//        $headers = array(
//            'Authorization: Bearer '.env('SECRET_KEY'),
//            'Content-type: Application/json'
//        );
//        $fields=[
//            'authorization_code'=>'AUTH_7g81wo9keo',
//            'amount'=>'100',
//            'email'=>'highdee.ai@gmail.com',
//            'send_invoices'=>false
//        ];
//        $url='https://api.paystack.co/transaction/charge_authorization';
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch,CURLOPT_POST,true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $result=curl_exec($ch);
//        curl_close($ch);
//        $data=json_decode($result);
//
//        if($data->status){
//            $txn=new transaction();
//            $txn->user_id=1;
//            $txn->amount=$data->data->amount;
//            $txn->text='made a savings of';
//            $txn->type='fixed_savins';
//            $txn->status=1;
//            $txn->reference=$data->data->reference;
//            $txn->save();
//        }
//
//        return $result;
//    }

    public function getSavings(Request $request){
        $user=Auth::user($request->query('token'));
        $savings=$user->fixedSavings()->get();

        return response()->json($savings);
    }

    public function getFixedSaving(Request $request,$id){
        $user=Auth::user($request->query('token'));
        $savings=fixedSavings::where(['id'=>$id,'user_id'=>$user->id])->where('status','!=','-1')->first();

        return $savings;
    }
    public function getFixedSavingTxn($id,$offset=0){
        $txns=fixedTransaction::where(['saving_id'=>$id])->offset($offset)->orderBy('id','desc')->paginate();

        return $txns;
    }
    public function update(Request $request , $id){
        $user=Auth::user($request->query('token'));
        $savings=fixedSavings::where(['id'=>$id,'user_id'=>$user->id])->first();

        if(!$savings){
            return response('',404);
        }

        $details=$request->input();
        $check=false;

        if(isset($details['amount'])){
            $savings->amount=$details['amount'];
        }
        if(isset($details['withdrawal_date'])){
            $savings->withdrawal_date=$details['withdrawal_date'];
        }
        if(isset($details['plan'])){
            $savings->plan=$details['plan'];
            $check=true;
        }
        if(isset($details['plan_day'])){
            $savings->plan_day=$details['plan_day'];
            $check=true;
        }
        if(isset($details['plan_date'])){
            $savings->plan_date=$details['plan_date'];
            $check=true;
        }
        if(isset($details['time'])){
            $savings->time=$details['time'];
            $check=true;
        }
        if(isset($details['card_id'])){
            $savings->card_id=$details['card_id'];
        }
        if(isset($details['label'])){
            $savings->title=$details['label'];
        }

        if(isset($details['status'])){
            $savings->status=$details['status'];

            $deposits=deposit::where("savings_id",$savings->id)->get();
            foreach ($deposits as $dp){
                $dp->delete();
            }
        }

        $savings->status=1; // making it active again so that we can check for more options in the generateDeposit method , if the new withdrawal date is still greater than the final withdrawal date then the generateDeposit method will return the status back to 2
        $savings->save();


        $this->generateDeposit($savings->id);

        return response($savings,200);
    }

    public function cancelWithdraw(Request $request,$id){
        $data=$request->input();
        $user=Auth::user($request->query('token'));

        $withd=withdrawal::where(['account_id'=>$id,'type'=>'fixed','status'=>0])->first();
        $fixed=fixedSavings::where(['user_id'=>$user->id,'id'=>$withd->fixedAccount()->first()->id])->first();

        if(!$fixed){
            return response([
                'status'=>false,
                'message'=>"Not allowed"
            ],401);
        }

        if(!$withd){
            return response([
                'status'=>false,
                'message'=>"No withdrawal found"
            ]);
        }

        $data=[
            'user'=> $user->lastname,
            'text'=>"
                <p>You have canceled your withdrawal request for ".$fixed->title." account</p>
            "
        ];

        Mail::to($user->email)->send(new notification($data));
        $withd->status=3; //3 means it was canceled by user
        $withd->save();

        return response([
            'status'=>true,
            'message'=>"Done"
        ]);
    }
}
