<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stash_transactions extends Model
{
    public function stash(){
        return $this->belongsTo('App\stash', 'stash_id','id');
    }
}
