<?php

namespace App\Console\Commands;

use App\banks;
use App\deposit;
use App\fixed;
use App\fixedTransaction;
use App\Http\Controllers\fixedSavingsController;
use App\transaction;
use App\User;
use Illuminate\Console\Command;

class fixedSavingsJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:fixed_savings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $headers = array(
//            'Authorization: Bearer '.env('SECRET_KEY'),
//            'Content-type: Application/json'
//        );
//        $url='https://api.paystack.co/bank';
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $result=curl_exec($ch);
//        curl_close($ch);
//        $data=json_decode($result);
//
//        foreach ($data->data as $bnk){
//            $bank=new banks();
//            $bank->name=$bnk->name;
//            $bank->code=$bnk->code;
//            $bank->long_code=$bnk->longcode;
//            $bank->save();
//        }
//        dd($data);

        //
       $deposits=deposit::where(['deposit_day'=>date('Y-m-d'),'deposit_at'=>((int)date('h')+1).date('a')])->get();

       foreach ($deposits as $key => $value){
           $card=$value->fixedSaving->card;
           $fixedSaving=$value->fixedSaving;

           if($fixedSaving->status ==  1){ //only active saving should be used
               $this->charge($fixedSaving);
               $value->delete();
           }
       }
    }

    public function charge($saving){
        $user=User::find($saving->user_id);

        $headers = array(
            'Authorization: Bearer '.env('SECRET_KEY'),
            'Content-type: Application/json'
        );
        $fields=[
            'authorization_code'=>$saving->card->authorization_code,
            'amount'=>$saving->amount * 100,
            'email'=>$user->email,
            'send_invoices'=>false
        ];
        $url='https://api.paystack.co/transaction/charge_authorization';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result=curl_exec($ch);
        curl_close($ch);
        $data=json_decode($result);


        if($data->status){ //if txn was successful
            $data->data->amount=$data->data->amount/100;
            $txn=new fixedTransaction();
            $txn->amount=$data->data->amount;
            $txn->text='made a savings of '.$saving->amount.' into '.$saving->title;
            $txn->saving_id=$saving->id;
            $txn->type='Credit';
            $txn->status=1;
            $txn->reference=$data->data->reference;
            $txn->save();

            //updating user fixed account status
            $fixedAccount=fixed::find($saving->fixed->id);
            $fixedAccount->amount+=$data->data->amount;
            $fixedAccount->save();

            //updating savings update
            $saving->last_withdrawal=date('Y-m-d');
            $saving->save();

            //resetting next withdrawal day
            $obj=new fixedSavingsController();
            $obj->generateDeposit($saving->id);
        }else{
            $txn=new fixedTransaction();
            $txn->amount=$data->data->amount;
            $txn->text='error occured while making payment of '.$saving->amount.' into '.$saving->title;
            $txn->saving_id=$saving->id;
            $txn->type='Credit';
            $txn->status=2;
            $txn->reference=$data->data->reference;
            $txn->save();
        }

//        return $result;
    }
}
