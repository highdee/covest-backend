<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\investment;

class investmentJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:investment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $investments=investment::where('status',0)->get();
        foreach ($investments as $investment){

            $tenor_data = json_decode($investment->data);
            $start = $investment->created_at;
            $end = $investment->created_at.'+'.(string)$tenor_data->months.' month';
            $diff = date_diff(date_create($start),date_create($end))->days;
            $start_current_date = date_diff(date_create($start),date_create(date('Y-m-d')))->days;
            $days_left=$diff - $start_current_date;

//            echo $start_current_date.'<br>';
            if($days_left <= 0){
                $investment->status=1;
                $investment->save();
            }
        }
    }
}
