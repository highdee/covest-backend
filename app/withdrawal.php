<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class withdrawal extends Model
{
    //

    protected $appends=[''];


    public function fixedAccount(){
        return $this->belongsTo('App\fixedSavings','account_id','id');
    }

    public function stashAccount(){
        return $this->belongsTo('App\stash', 'account_id', 'id');
    }
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
