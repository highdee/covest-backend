<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stash extends Model
{
    //

    protected $appends=['hasActiveWithdrawal'];

    public function getHasActiveWithdrawalAttribute(){
        return $this->hasActiveWithdrawal();
    }

    public function hasActiveWithdrawal(){
        return withdrawal::where(['account_id'=>$this->id,'type'=>'stash','status'=>0])->first() ? true:false;
    }

    public function stash_transactions(){
        return $this->hasMany('App\stash_transactions', 'stash_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
