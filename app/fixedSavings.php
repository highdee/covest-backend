<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;

class fixedSavings extends Model
{
    //

    protected $appends=['fixed','card','since','hasActiveWithdrawal'];

    public function getHasActiveWithdrawalAttribute(){
        return $this->hasActiveWithdrawal();
    }

    public function getSinceAttribute(){
        return $this->since();
    }

    public function getFixedAttribute(){
        return $this->fixed()->first();
    }

    public function getCardAttribute(){
        return $this->card()->first();
    }

    public function card(){
        return $this->belongsTo('App\card','card_id','id');
    }

    public function fixed(){
        return $this->belongsTo('App\fixed','id','fixed_savings_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    
    public function deposits(){
        return $this->hasMany('App\deposit','savings_id','id');
    }
    public function since(){
        $date=strtotime($this->created_at);
        $current=strtotime(date('Y-m-d h:i:s'));
        $diff=abs($current - $date);

        return $diff/((60*60*(24*30)));
    }

    public function hasActiveWithdrawal(){
        return withdrawal::where(['account_id'=>$this->id,'type'=>'fixed','status'=>0])->first() ? true:false;
    }


}
