<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class admin extends Authenticatable
{
    //
    use Notifiable;

    protected $guard ="admin";

    protected $fillable=['username','password'];
    protected $hidden=['password'];
}
