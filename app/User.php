<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','covest_password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends=['stash','fixed','card','has_password','stashWithdrawal','banks'];

    public function getBanksAttribute(){
        return $this->banks();
    }

    public function getStashWithdrawalAttribute(){
        return $this->withdrawals()->where(['status'=>0,'type'=>'stash'])->first();
    }
    public function getHasPasswordAttribute(){
        return $this->covest_password != null ? true:false;
    }
    public function getStashAttribute(){
        return $this->stash()->first();
    }
    public function getFixedAttribute(){
        return $this->fixed()->first();
    }
    public function getCardAttribute(){
        return $this->cards()->get();
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function withdrawals(){
        return $this->hasMany('App\withdrawal','user_id','id');
    }

    public function investments(){
        return $this->hasMany('App\investment','user_id','id');
    }

    public function stash(){
        return $this->belongsTo('App\stash','id','user_id');
    }

    public function fixed(){
        return $this->belongsTo('App\fixed','id','user_id');
    }
    public function fixedSavings(){
        return $this->hasMany('App\fixedSavings','user_id','id');
    }
    public function cards(){
        return $this->hasMany('App\card','user_id','id');
    }
   
    public function banks(){
        return banks::all();
    }
}
