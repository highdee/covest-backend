<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class investment extends Model
{
    //

    protected  $appends=['start','ends','roi','profit','ready','hasActiveWithdrawal'];

    public function getHasActiveWithdrawalAttribute(){
        return $this->hasActiveWithdrawal();
    }

    public function  getStartAttribute(){
        return $this->start();
    }
    public function  getEndsAttribute(){
        return $this->end();
    }
    public function getProfitAttribute(){
        return number_format($this->profit(),2);
    }
    public function getReadyAttribute(){
        return $this->ready();
    }
    public function  getRoiAttribute(){
        return $this->roi();
    }
    public function start(){
        return date('D, M-d-Y',strtotime( $this->created_at));
    }
    public function end(){
        $tenor_data=json_decode($this->data);
        return date('D, M-d-Y',strtotime($this->created_at.'+'.(string)$tenor_data->months.' month'));
    }
    public function roi(){
        $tenor_data=json_decode($this->data);
        return $tenor_data->percentage;
    }
    public function profit(){
        $tenor_data = json_decode($this->data);
        $start = $this->created_at;
        $end = $this->created_at.'+'.(string)$tenor_data->months.' month';
        $diff = date_diff(date_create($start),date_create($end))->days;
        $profit = ($tenor_data->percentage / 100 ) * $this->amount;
        $profit_per_day = $profit / $diff;
        $start_current_date = date_diff(date_create($start),$this->status == 0 ? date_create(date('Y-m-d')) : date_create($end))->days;
        $current_profit = $profit_per_day * $start_current_date;

        return $current_profit;
    }

    public function ready(){
        $tenor_data = json_decode($this->data);
        $start = $this->created_at;
        $end = $this->created_at.'+'.(string)$tenor_data->months.' month';
        $diff = date_diff(date_create($start),date_create($end))->days;
        $start_current_date = date_diff(date_create($start),date_create(date('Y-m-d')))->days;
        $days_left=$diff - $start_current_date;

        return $days_left;
    }



    public function hasActiveWithdrawal(){
        return withdrawal::where(['account_id'=>$this->id,'type'=>'invest','status'=>0])->first() ? true:false;
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id','id');
    }
}
