@extends('inc.admin_asset')
@section('tenorActive')
    active
@endsection

@section('content')
    <div class="row">
        @include('inc.notification_display')
        <form action="withdrawal-token" method="post">
            <div class="form-group">
                <label for="">ENTER TOKEN</label>
                <input type="text" name="code" class="form-control" placeholder="">
            </div>
            <button class="btn btn-success">SUBMIT</button>
        </form>
    </div>

@endsection
