@extends('inc.admin_asset')
@section('fixedActive')
	active
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Fixed Account</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/covestAdmin-page/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Fixed Account List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')   
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        {{-- <div class="card-header">
                            <h4 class="card-title">Zero configuration</h4>
                        </div> --}}
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                {{-- <p class="card-text">DataTables has most features enabled by default, so all you need to do to
                                    use it with your own tables is to call the construction function: $().DataTable();.</p> --}}
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Title</th>
                                                <th>Amount</th>
                                                <th>Plan</th>
                                                <th>Withdrawal Date</th>
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($fixed as $key => $val)
                                                <tr onclick="window.location.href='/covestAdmin-page/user_show/{{$fixed[$key]->user->id}}'">                                
                                                    <td>{{$fixed[$key]->user->firstname}} {{$fixed[$key]->user->lastname}}</td>
                                                    <td>{{$val->title}}</td>
                                                    <td>{{$val->amount}}</td>
                                                    <td> {{$val->plan}}</td> 
                                                    <td> {{$val->withdrawal_date}}</td> 
                                                    {{-- <td class="text-center"><a href="/covestAdmin-page/deleteUser/{{$user->id}}"><i class="bx bx-trash"></i></a></td>           --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Title</th>
                                                <th>Amount</th>
                                                <th>Plan</th>
                                                <th>Withdrawal Date</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection