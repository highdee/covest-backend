@extends('inc.admin_asset')
@section('tenorActive')
	active
@endsection

@section('content')
    <button type="button" class="btn btn-primary mr-1 mb-1 pl-3 pr-3" data-toggle="modal" data-target="#default"><i class="bx bx-plus"></i>Add Tenor</button>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Tenors</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/covestAdmin-page/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Tenor
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')   
        <div class="row"  >
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel1">Add New Tenor</h3>
                            <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <section id="multiple-column-form">
                                <div class="row match-height">
                                    <div class="col-12">
                                        <div class="card-content">
                                           
                                            <div class="card-body p-0">
                                                <form class="form" method="post" action="/covestAdmin-page/create_tenor">
                                                    @csrf
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12 col-12">
                                                                <label for="u-name-column">Title</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="u-name-column" class="form-control" placeholder="Title" name="title">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 mb-1">
                                                                <label for="per">Percentage</label>
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="number" min="0" id="per" max="100" name="percentage" class="form-control" placeholder="Percentage" aria-describedby="basic-addon2">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text" id="basic-addon2">%</span>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-12 mb-1">
                                                                <label for="months">Months</label>
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="number" id="months" min="1" max="12" name="months" class="form-control" placeholder="Months" aria-describedby="basic-addon2">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text" id="basic-addon2">months</span>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <label for="address">Description</label>
                                                                <fieldset class="form-group">
                                                                    <textarea class="form-control" name="description" id="address" rows="3" placeholder="Description"></textarea>
                                                                </fieldset>
                                                            </div>

                                                            <div class="col-12 d-flex justify-content-end">
                                                                <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Percentage</th>
                                                <th>Month</th>
                                                <th>Description</th>
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($tenor as $key => $t)
                                                <tr>
                                                    <td>{{$t->title}} </td>
                                                    <td>{{$t->percentage}}</td>
                                                    <td>{{$t->months}}</td>
                                                    <td>{{$t->description}}</td>  
                                                    <td>
                                                        <form action="/covestAdmin-page/deleteTenor" method="Post">
                                                            @csrf
                                                            {{-- <input type="hidden" value="{{$vendors[$key]->owner['id']}}" name="user_id">
                                                            <input type="submit" class="btn {{ $vendor->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $vendor->status == 0 ? 'active' : 'banned'}}" > --}}
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Title</th>
                                                <th>Percentage</th>
                                                <th>Month</th>
                                                <th>Description</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection