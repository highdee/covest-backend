@extends('inc.admin_asset')
@section('investmentActive')
	active
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Investment</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/covestAdmin-page/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Investment List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')   
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Label</th>
                                                <th>Tenor</th>
                                                <th>Amount</th>
                                                <th>Ref</th>
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($investment as $key => $val)
                                                <tr onclick="window.location.href='/covestAdmin-page/user_show/{{$investment[$key]->user->id}}'">                                
                                                    <td>{{$investment[$key]->user->firstname}} {{$investment[$key]->user->lastname}}</td>
                                                    <td>{{$val->label}}</td>
                                                    <td>{{$val->tenor}}</td>
                                                    <td>{{$val->amount}}</td>
                                                    <td>{{$val->ref}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Label</th>
                                                <th>Tenor</th>
                                                <th>Amount</th>
                                                <th>Data</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection