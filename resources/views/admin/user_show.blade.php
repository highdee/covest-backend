@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <!-- page user profile start -->
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <!-- user profile heading section start -->
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">User</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="fixed-tab" data-toggle="tab" href="#fixed" aria-controls="fixed" role="tab" aria-selected="false"><i class="bx bx-copy-alt"></i><span class="d-none d-md-block">Fixed Savings</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="stash-tab" data-toggle="tab" href="#stash" aria-controls="stash" role="tab" aria-selected="false"><i class="bx bx-stats"></i><span class="d-none d-md-block">Stash Saving</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="withdraw-tab" data-toggle="tab" href="#withdraw" aria-controls="withdraw" role="tab" aria-selected="false"><i class="bx bx-images"></i><span class="d-none d-md-block">Withdrawal</span></a>
                                    </li>
                                     <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="investment-tab" data-toggle="tab" href="#investment" aria-controls="investment" role="tab" aria-selected="false"><i class="bx bx-line-chart"></i><span class="d-none d-md-block">Investment</span></a>
                                    </li>
                                    {{--<li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="comments-tab" data-toggle="tab" href="#comments" aria-controls="comments" role="tab" aria-selected="false"><i class="bx bx-message-alt"></i><span class="d-none d-md-block">Comments</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="bookings-tab" data-toggle="tab" href="#bookings" aria-controls="bookings" role="tab" aria-selected="false"><i class="bx bx-book-open"></i><span class="d-none d-md-block">Bookings</span></a> 
                                    </li>--}}
                                </ul>
                            </div>
                            <!-- user profile nav tabs ends -->
                        </div>
                    </div>
                    <!-- user profile heading section ends -->

                    <!-- user profile content section start -->
                    @include('inc.notification_display')
                    <div class="row">
                        <!-- user profile nav tabs content start -->
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <h5 class="card-title">Basic details</h5>
                                                <ul class="list-unstyled">
                                                    <h6><li><i class="bx bx-user mr-50 mb-1"></i>{{$user->firstname}} {{$user->lastname}}</li></h6>
                                                    {{-- <h6><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->address}}</li></h6>
                                                    <h6><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->city}}</li></h6> --}}
                                                    <h6><li><i class="cursor-pointer bx bx-phone-call mb-1 mr-50"></i>{{$user->phone}} </li></h6>
                                                    <h6><li><i class="cursor-pointer bx bx-envelope mb-1 mr-50"></i>{{$user->email}}</li></h6>
                                                </ul>
                                                <div>
                                                    <form action="/covestAdmin-page/banUser" method="Post">
                                                        @csrf
                                                        <input type="hidden" value="{{$user->id}}" name="user_id">
                                                        <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'active' : 'banned'}}" >
                                                        
                                                        <a href="/covestAdmin-page/deleteUser/{{$user->id}}" class="btn btn-danger">Delete</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane container-fluid" id="fixed" aria-labelledby="fixed-tab" role="tabpanel">
                                    @if(count($user->fixedSavings) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Fixed Saving Available
                                                </span>
                                            </div>
                                        </div>
                                    @else    
                                        <div class="row">
                                        
                                            @foreach($user->fixedSavings as $fix => $value)
                                                <div class="col-md-5">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body pl-0 pr-0">
                                                                <div class="col-12">
                                                                    <div class="" style="display:flex;justify-content: space-between">
                                                                        <div class="title" style="">
                                                                            <h6 class="pb-0 mb-0">Name of Saving</h6>
                                                                            <p class="">{{$value->title}}</p>
                                                                        </div>
                                                                        <div class="title" style="">
                                                                            <h6 class="pb-0 mb-0">Deposit per Time</h6>
                                                                            <p class="mt-0">{{$value->amount}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="text-center mb-2">
                                                                        <h6 class="">Amount Available</h6>
                                                                        <h3>NGN {{$value->fixed->amount}}</h3>
                                                                    </div>
                                                                    <div class="freq text-center">
                                                                        <p class="mb-0" style="text-transform:capitalize;background-color:#f1f1f1; border-radius:10px">{{$value->plan}} autosave Deposit</p>
                                                                    </div>
                                                                    <div class="mt-2" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Last Deposit</h6>
                                                                        <div class="badge badge-light-danger text-bold-500 py-50">{{$user->fixedSavings[$fix]->deposits[0]->deposit_day}}</div>
                                                                    </div>
                                                                    <div class="mt-1" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Next Deposit</h6>
                                                                        <div class="badge badge-light-primary text-bold-500 py-50">{{$value->next_withdrawal}}</div>
                                                                    </div>
                                                                    <div class="mt-1" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Withdrawal Date</h6>
                                                                        <div class="badge badge-light-success text-bold-500 py-50">{{$value->withdrawal_date}}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            @endforeach      
                                            
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4 class="card-title">Fixed Transaction</h4>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="card-body card-dashboard">
                                                            <div class="table-responsive">
                                                                <table class="table zero-configuration">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Amount</th>
                                                                            <th>Text</th>
                                                                            <th>Type</th>
                                                                            <th>Reference</th>
                                                                            <th>Created</th>
                                                                    </thead>
                                                                    <tbody class="table-hover">
                                                                        @foreach($user->fixedSavings as $k => $ft)
                                                                            @if (count($ft->fixed->fixedTransactions) < 1)
                                                                                
                                                                            @else
                                                                                <tr>                                
                                                                                    <td>{{$ft->fixed->fixedTransactions[0]->amount}}</td>
                                                                                    <td>{{$ft->fixed->fixedTransactions[0]->text}}</td>
                                                                                    <td>{{$ft->fixed->fixedTransactions[0]->type}}</td>
                                                                                    <td>{{$ft->fixed->fixedTransactions[0]->reference}}</td>
                                                                                    <td>{{$ft->fixed->fixedTransactions[0]->created_at}}</td>
                                                                                </tr>
                                                                            @endif
                                                                            
                                                                        @endforeach
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Text</th>
                                                                            <th>Type</th>
                                                                            <th>Data</th>
                                                                            <th>Created</th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="stash" aria-labelledby="stash-tab" role="tabpanel">
                                    @if(empty($user->stash))
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Stash Saving Available
                                                </span>
                                            </div>
                                        </div>
                                    @else    
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <div class="card-body pl-0 pr-0">
                                                            <div class="col-12">
                                                                <div class="" style="display:flex;justify-content: space-between">
                                                                    <div class="title" style="">
                                                                        <h6 class="pb-0 mb-0">Amount</h6>
                                                                        <p class="">{{$user->stash->amount}}</p>
                                                                    </div>
                                                                    <div class="title" style="">
                                                                        <h6 class="pb-0 mb-0">Type</h6>
                                                                        <p class="mt-0">{{count($user->stash->stash_transactions) < 1 ? '' : $user->stash->stash_transactions[0]->type}}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-2" style="display:flex;justify-content: space-between">
                                                                    <h6 class="">Reference</h6>
                                                                    <div class="">{{count($user->stash->stash_transactions) < 1 ? '' : $user->stash->stash_transactions[0]->reference}}</div>
                                                                </div>
                                                                <div class="mt-1" style="display:flex;justify-content: space-between">
                                                                    <h6 class="">Account Created Date</h6>
                                                                    <div class="badge badge-light-success text-bold-500 py-50">{{$user->stash->created_at}}</div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4 class="card-title">Stash Transaction</h4>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="card-body card-dashboard">
                                                            <div class="table-responsive">
                                                                <table class="table zero-configuration">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Amount</th>
                                                                            <th>Text</th>
                                                                            <th>Type</th>
                                                                            <th>Reference</th>
                                                                            <th>Created</th>
                                                                    </thead>
                                                                    <tbody class="table-hover">
                                                                        @foreach($user->stash->stash_transactions as $k => $st)
                                                                            <tr>                                
                                                                                <td>{{$st->amount}}</td>
                                                                                <td>{{$st->text}}</td>
                                                                                <td>{{$st->type}}</td>
                                                                                <td>{{$st->reference}}</td>
                                                                                <td>{{$st->created_at}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Text</th>
                                                                            <th>Type</th>
                                                                            <th>Data</th>
                                                                            <th>Created</th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="withdraw" aria-labelledby="withdraw-tab" role="tabpanel">
                                    @if(count($user->withdrawals) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Withdrawal Available
                                                </span>
                                            </div>
                                        </div>
                                    @else    
                                        <div class="row">
                                        
                                            @foreach($user->withdrawals as $withd => $value)
                                                <div class="col-md-4">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body pl-0 pr-0">
                                                                <div class="col-12">
                                                                    <div class="" style="display:flex;justify-content: space-between">
                                                                        <div class="title" style="">
                                                                            <h6 class="pb-0 mb-0">Amount</h6>
                                                                            <p class="">NGN {{$value->amount}}</p>
                                                                        </div>
                                                                        <div class="title" style="">
                                                                            <h6 class="pb-0 mb-0">Type</h6>
                                                                            <p class="mt-0">{{$value->type}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Withdrawal was made on</h6>
                                                                        <div class="badge badge-light-primary text-bold-500 py-50">{{$value->created_at}}</div>
                                                                    </div>
                                                                    <div class="row">
                                                                        @if($value->status == 1 )
                                                                            <div class="col-12">
                                                                                <div class="badge badge-light-success text-bold-500 py-50">Payment Approved</div>
                                                                            </div>
                                                                        @else
                                                                            <div class="col-6">
                                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#d{{$value->id}}">Show Details</button>
                                                                            </div>
                                                                            <div class="col-6">
                                                                                <input type="submit" data-toggle="modal" data-target="#c{{$value->id}}" class="form-control btn btn-danger" value="{{$value->status == -2 ? 'Cancelled By Admin' : 'Cancel'}}" {{$value->status == -2 ? 'disabled' : ''}}>
                                                                            </div>
                                                                            <div class="col-12 mt-1">
                                                                                <input type="submit" data-toggle="modal" data-target="#s{{$value->id}}" class="form-control btn btn-success" value="{{$value->status == 3 ? 'Cancelled By User' : 'Pay'}}" {{$value->status != 0 ? 'disabled' : ''}}>
                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                    <div class="modal-primary mr-1 mb-1 d-inline-block">
                                                                        <div class="modal fade text-left" id="d{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel110" aria-hidden="true" style="display: none;">
                                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header bg-primary">
                                                                                        <h5 class="modal-title white" id="myModalLabel110">Withdrawal Details</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <i class="bx bx-x"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Name</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">{{$user->firstname}} {{$user->lastname}}</div>
                                                                                        </div>

                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Amount Requested</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">NGN {{$value->amount}}</div>
                                                                                        </div>
                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Type</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">{{$value->type}}</div>
                                                                                        </div>
                                                                                        @if($value->type == 'fixed')
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Amount Available</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">NGN {{$value->fixedAccount->fixed->amount}}</div>
                                                                                            </div>
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Title</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">{{$value->fixedAccount->title}}</div>
                                                                                            </div>
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Account Plan</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">{{$value->fixedAccount->plan}}</div>
                                                                                            </div>
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Withdrawal Date</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">{{$value->fixedAccount->withdrawal_date}}</div>
                                                                                            </div>
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Last Withdrawal Date</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">{{$value->fixedAccount->last_withdrawal}}</div>
                                                                                            </div>
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Account Created Date</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">{{$value->fixedAccount->created_at}}</div>
                                                                                            </div>
                                                                                        @elseif($value->type = 'stash')
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Amount Available</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">NGN {{$value->stashAccount->amount}}</div>
                                                                                            </div>
                                                                                            <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                                <h6 class="">Account Created Date</h6>
                                                                                                <div class="badge badge-light-primary text-bold-500 py-50">{{$value->stashAccount->created_at}}</div>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal-success mr-1 mb-1 d-inline-block">
                                                                        <div class="modal fade text-left" id="s{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel110" aria-hidden="true" style="display: none;">
                                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header bg-success">
                                                                                        <h5 class="modal-title white" id="myModalLabel110">Confirm Withdrawal</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <i class="bx bx-x"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Name</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">{{$user->firstname}} {{$user->lastname}}</div>
                                                                                        </div>

                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Amount</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">NGN {{$value->amount}}</div>
                                                                                        </div>
                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Type</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">{{$value->type}}</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <form action="/covestAdmin-page/withdrawal" method="post">
                                                                                            @csrf
                                                                                            <input type="hidden" name="withdraw_id" value="{{$value->id}}">
                                                                                            <input type="submit" class="form-control btn btn-success" value="{{$value->status == -1 ? 'Cancelled By User' : 'Confirm Payment'}}" {{$value->status != 0 ? 'disabled' : ''}}>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal-danger mr-1 mb-1 d-inline-block">
                                                                        <div class="modal fade text-left" id="c{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel110" aria-hidden="true" style="display: none;">
                                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header bg-danger">
                                                                                        <h5 class="modal-title white" id="myModalLabel110">Confirm Withdrawal Cancellation</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <i class="bx bx-x"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        Confirm Payment Cancellation of NGN {{$value->amount}} to {{$user->firstname}} {{$user->lastname}} made from {{$value->type}} Account 
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <form action="/covestAdmin-page/cancelWithdrawal" method="post">
                                                                                            @csrf
                                                                                            <input type="hidden" name="withdraw_id" value="{{$value->id}}">
                                                                                            <input type="submit" class="form-control btn btn-danger" value="Cancel Withdrawal">
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            @endforeach         
                                        </div>
                                    @endif
                                </div>

                                <div class="tab-pane container-fluid" id="investment" aria-labelledby="investment-tab" role="tabpanel">
                                    @if(count($user->investments) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Investment Available
                                                </span>
                                            </div>
                                        </div>
                                    @else    
                                        <div class="row">
                                        
                                            @foreach($user->investments as $inv => $val)
                                                <div class="col-md-4">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body pl-0 pr-0">
                                                                <div class="col-12">
                                                                    <div class="" style="display:flex;justify-content: space-between">
                                                                        <div class="title" style="">
                                                                            <h6 class="pb-0 mb-0">Label</h6>
                                                                            <p class=""> {{$val->label}}</p>
                                                                        </div>
                                                                        <div class="title" style="">
                                                                            <h6 class="pb-0 mb-0">Amount</h6>
                                                                            <p class="mt-0">NGN {{$val->amount}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Tenor</h6>
                                                                        <div class="badge badge-light-primary text-bold-500 py-50">{{$val->tenor}}</div>
                                                                    </div>
                                                                    <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Reference</h6>
                                                                        <div class="badge badge-light-primary text-bold-500 py-50">{{$val->ref}}</div>
                                                                    </div>
                                                                    <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                        <h6 class="">Created On</h6>
                                                                        <div class="badge badge-light-primary text-bold-500 py-50">{{$val->created_at}}</div>
                                                                    </div>
                                                                    <div class="row">
                                                                        @if ($val->status == 2)
                                                                            <div class="col-12">
                                                                                <div class="badge badge-light-primary text-bold-500 py-50">Admin Approved Payment of Investment</div>
                                                                            </div>
                                                                        
                                                                        @else
                                                                            @if($val->status == 1)
                                                                                <div class="col-6">
                                                                                    <input type="submit" data-toggle="modal" data-target="#s{{$val->ref}}" class="form-control btn btn-success" value="Pay Investment" >
                                                                                </div>
                                                                            @endif
                                                                            <div class="col-6">
                                                                                <input type="submit" data-toggle="modal" data-target="#c{{$val->ref}}" class="form-control btn btn-danger" value="{{$val->status == -2 ? 'Cancelled By Admin' : 'Cancel'}}" {{$val->status == -2 ? 'disabled' : ''}}>
                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                    <div class="modal-success mr-1 mb-1 d-inline-block">
                                                                        <div class="modal fade text-left" id="s{{$val->ref}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel110" aria-hidden="true" style="display: none;">
                                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header bg-success">
                                                                                        <h5 class="modal-title white" id="myModalLabel110">Confirm Investment Withdrawal</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <i class="bx bx-x"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Name</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">{{$user->firstname}} {{$user->lastname}}</div>
                                                                                        </div>

                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Amount</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">NGN {{$val->amount}}</div>
                                                                                        </div>
                                                                                        <div class="mt-2 mb-2" style="display:flex;justify-content: space-between">
                                                                                            <h6 class="">Tenor</h6>
                                                                                            <div class="badge badge-light-primary text-bold-500 py-50">{{$val->tenor}}</div>
                                                                                        </div>  
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <form action="/covestAdmin-page/withdrawal" method="post">
                                                                                            @csrf
                                                                                            <input type="hidden" value="{{$val->id}}" name="invest_id">
                                                                                            <input type="submit" class="form-control btn btn-success" value="Comfirm Investment Payment">
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="modal-danger mr-1 mb-1 d-inline-block">
                                                                        <div class="modal fade text-left" id="c{{$val->ref}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel110" aria-hidden="true" style="display: none;">
                                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header bg-danger">
                                                                                        <h5 class="modal-title white" id="myModalLabel110">Confirm Withdrawal Cancellation</h5>
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <i class="bx bx-x"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        Confirm Investment Payment Cancellation of NGN {{$val->amount}} to {{$user->firstname}} {{$user->lastname}} on  {{$val->tenor}} tenor investment
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <form action="/covestAdmin-page/cancelInvestment" method="post">
                                                                                            @csrf
                                                                                            <input type="hidden" name="invest_id" value="{{$val->id}}">
                                                                                            <input type="submit" class="form-control btn btn-danger" value="Cancel Investment Withdrawal">
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            @endforeach         
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
