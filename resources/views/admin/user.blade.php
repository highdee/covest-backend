@extends('inc.admin_asset')
@section('userActive')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/covestAdmin-page/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">User List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Email</th>
                                                <th>Active</th>
                                                <th>Delete</th>
                                            </tr>  
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($users as $user)
                                                {{-- onclick="window.location.href='/covestAdmin-page/user_show/{{$user->id}}'" --}}
                                                <tr onclick="window.location.href='/covestAdmin-page/user_show/{{$user->id}}'">                                
                                                    <td>{{$user->firstname}} {{$user->lastname}}</td>
                                                    <td>{{$user->phone}}</td>
                                                    <td>{{$user->email}}</td>
                                                    <td class="text-center">
                                                        <form action="/covestAdmin-page/banUser" method="post">
                                                            @csrf
                                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                                            <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'Active' : 'Banned'}}">
                                                        </form> 
                                                    </td> 
                                                    <td class="text-center"><a href="/covestAdmin-page/deleteUser/{{$user->id}}"><i class="bx bx-trash"></i></a></td>          
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Email</th>
                                                <th>Active</th>
                                                <th>Delete</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection