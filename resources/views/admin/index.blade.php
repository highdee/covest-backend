@extends('inc.admin_asset')
@section('adminHome')
	active
@endsection
@section('content')
	<div class="content-header row">
	</div>
	<div class="content-body">
		<!-- Dashboard Ecommerce Starts -->
		<section id="dashboard-ecommerce">
			<div class="row">
				<div class="col-xl-4 dashboard-users">
					<div class="dashboard-users-danger">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
										<i class="bx bx-user font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Users</div>
									<h3 class="mb-0">{{$user}}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				{{-- <div class="col-xl-4 dashboard-users">
					<div class="dashboard-users-danger">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
										<i class="bx bx-user font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Vendors</div>
									<h3 class="mb-0"></h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-12 dashboard-users">
					<div class="dashboard-users-danger">
						<div class="card text-center">
							<div class="card-content">
								<div class="card-body py-1">
									<div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
										<i class="bx bx-user font-medium-5"></i>
									</div>
									<div class="text-muted line-ellipsis">Salons</div>
									<h3 class="mb-0"></h3>
								</div>
							</div>
						</div>
					</div>
				</div> --}}
			</div>
		</section>
@endsection