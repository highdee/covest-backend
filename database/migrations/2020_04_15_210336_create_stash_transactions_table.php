<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStashTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stash_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount');
            $table->string('text');
            $table->integer('stash_id');
            $table->integer('status');
            $table->string('type');
            $table->string('reference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stash_transactions');
    }
}
