<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedSavingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_savings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('title');
            $table->float('amount');
            $table->integer('card_id');
            $table->string('plan');
            $table->string('plan_date')->nullable();
            $table->string('plan_day')->nullable();
            $table->string('time');
            $table->date('withdrawal_date');
            $table->date('last_withdrawal')->nullable();
            $table->date('next_withdrawal')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_savings');
    }
}
