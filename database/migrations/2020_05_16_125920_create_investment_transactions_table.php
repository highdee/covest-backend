<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('investment_transactions', function (Blueprint $table) {
//            $table->bigIncrements('id');
//            $table->float('amount');
//            $table->string('text');
//            $table->integer('saving_id');
//            $table->integer('status');
//            $table->string('type');
//            $table->string('reference');
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_transactions');
    }
}
