<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Card extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('cards',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('account_name');
            $table->string('last4');
            $table->string('exp_month');
            $table->string('exp_year');
            $table->string('card_type');
            $table->string('reference');
            $table->string('customer_code');
            $table->string('authorization_code');

            $table->integer('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
