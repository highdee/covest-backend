<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('/v1/')->group(function(){
    Route::post('login',"loginController@login");
    Route::post('create-account',"loginController@create_account");
    Route::get('verify-account',"loginController@verify_account");


    Route::post('forget-password',"loginController@forget_password");
    Route::post('reset-password',"loginController@reset_password");
    Route::get('verify-reset-token',"loginController@verify_password");

    Route::get('refresh-token/{id}',"userController@reset_token");
    Route::get('resend-verification/{id}',"loginController@resendVerificationCode");

    Route::get('get-user',"userController@get_user");
    Route::get('get-dashboard',"userController@get_dashboard");
    Route::get('get-cards',"userController@get_user_card");


    Route::post('add-new-card',"cardController@add");
    Route::get('get-cards',"cardController@getCards");
    Route::get('delete-card/{id}',"cardController@deleteCard");



    Route::post('add-fixed-saving','fixedSavingsController@create');
    Route::post('update-fixed-saving/{id}','fixedSavingsController@update');
    Route::get('get-fixed-saving/{id}','fixedSavingsController@getFixedSaving');
    Route::get('get-fixed-saving-txn/{id}','fixedSavingsController@getFixedSavingTxn');
    Route::get('get-fixed-savings','fixedSavingsController@getSavings');
    Route::post('fixed-withdraw',"fixedSavingsController@withdraw");
    Route::post('cancel-fixed-withdraw/{id}',"fixedSavingsController@cancelWithdraw");



    Route::get('get-stash-saving','stashSavingsController@getStash');
    Route::get('create-stash-saving','stashSavingsController@createStash');
    Route::get('get-stash-saving-txn/{id}','stashSavingsController@getStashSavingTxn');
    Route::post('create-stash-saving','stashSavingsController@saveStashSaving');
    Route::get('save-ref-stash-saving/{ref}','stashSavingsController@recordSaved');
    Route::post('stash-withdraw',"stashSavingsController@withdraw");
    Route::post('cancel-stash-withdraw/{id}',"stashSavingsController@cancelWithdraw");



    Route::post('set-pin',"userController@set_pin");
    Route::post('change-password',"userController@change_password");
    Route::post('update-profile',"userController@update_profile");

//    Route::get('test/{id}','fixedSavingsController@generateDeposit');


    Route::get('get-tenors','investmentController@getTenors');
    Route::post('create-investment',"investmentController@create_investment");
    Route::get('get-investments',"investmentController@get_investment");
    Route::post('invest-withdraw',"investmentController@invest_withdraw");

    Route::prefix('webhook/event')->group(function(){
        Route::post('success','webhookEventController@success_event');
    });
});

