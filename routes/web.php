<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();



Route::prefix('/covestAdmin-page')->group(function(){
    Route::get('/adminLogin', 'adminLoginController@show');
    Route::post('/adminLogin', 'adminLoginController@login');
    Route::get('/admin', 'adminController@index');
    Route::get('/userList', 'adminController@userList')->name('admin.user');
    Route::get('/user_show/{id}', 'adminController@user_show');
    Route::get('/deleteUser/{id}', 'adminController@deleteUser');
    Route::post('/banUser', 'adminController@banUser');
    Route::get('/adminLogout', 'adminController@logout');
    Route::get('/fixedList', 'adminController@fixedList');
    Route::get('/stashList', 'adminController@stashList');
    Route::get('/withdrawalList', 'adminController@withdrawalList');
    Route::post('/withdrawal', 'adminController@withdrawal');
    Route::post('/withdrawal-token', 'adminController@withdrawalToken');
    Route::post('/cancelWithdrawal', 'adminController@cancelWithdrawal');
    Route::get('/tenors','adminController@tenors');
    Route::post('/create_tenor', 'adminController@create_tenor');
    Route::get('/investment', 'adminController@investment');
    Route::post('/withdrawInvestment', 'adminController@withdrawInvestment');
    Route::post('/cancelInvestment', 'adminController@cancelInvestment');
});
